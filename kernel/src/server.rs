use collections::treemap::TreeMap;
use serialize::json;
use serialize::json::ToJson;
use std::collections::HashMap;
use std::io::fs::readdir;
use std::num::{FromStrRadix};
use time::get_time;
use uuid::Uuid;

use super::auth;
use super::lua;
use super::lua::{LuaResult};
use super::lua::{LuaValue};
use super::objects;
use super::objects::{Script};

pub struct Session {
	lua: lua::LuaState,
	auth_state: Option<auth::AuthState>,
	scripts: Vec<objects::Script>
}

fn json_get_string(x: &json::Object, k: &String) -> Option<String> {
	match x.find(k) {
		Some(&json::String(ref s)) => { return Some(s.clone()); }
		_ => { return None; }
	}
}

macro_rules! try_get_param(
	($json:expr, $field:expr) => (
		match json_get_string($json, &String::from_str($field)) {
			Some(s) => s,
			_ => fail!("'{}' field missing", $field)
		}
	);
)

macro_rules! nyi(
	() => (fail!("not yet implemented"));
)

macro_rules! err(
	($msg:expr $(, $arg:tt)*) => (
		return Err(format!($msg $(, $arg)*))
	)
)

impl Session {
	pub fn new(initial_scripts: &Vec<objects::Script>) -> Session {
		let mut session = Session {
			lua: lua::LuaState::new(::std::ptr::mut_null()),
			auth_state: None,
			scripts: initial_scripts.clone()
		};

		for script in initial_scripts.iter() {
			session.lua_load_script(script);
		}

		return session;
	}

	pub fn lua_list_scripts(&self) -> Vec<String> {
		let mut script_paths = Vec::new();
		for s in self.scripts.iter() {
			let filename = match s.path {
				Some(ref p) => match p.filename_str() {
					Some(ref fp) => String::from_str(fp.clone()),
					_ => continue
				},
				_ => continue,
			};
			script_paths.push(filename);
		}
		return script_paths;
	}

	pub fn lua_load_script(&mut self, script: &objects::Script) -> LuaResult {
		script.load_into(&self.lua)
	}

	pub fn lua_save_script(&self, script : &mut Script, destfilename : Option<String>) ->
			Result<String, String> {
		let filename = match destfilename {
			Some(s) => format!("scripts/{}.lua", s),
			_ => Server::autogen_script_path()
		};

		match script.save(Some(&Path::new(filename.clone()))) {
			Ok(_) => return Ok(filename.clone()),
			Err(_) => return Err(String::from_str("Unable to save lua file")),
		}
	}

	pub fn lua_eval(&self, message : String) ->
			Vec<LuaValue> {
		self.lua.eval(message.as_slice());
		return self.lua.pop_all();
	}

	pub fn lua_save_request(&self, json: &json::Object,
			_: &RequestContext) ->
			Result<Option<json::Object>, String> {
		let mut reply = TreeMap::new();
		reply.insert(String::from_str("type"),
			json::String(String::from_str("luaSaveResult")));

		let message = match json_get_string(json, &String::from_str("message")) {
			Some(s) => s,
			_ => return Err(String::from_str("'message' field missing"))
		};

		let filename = json_get_string(json, &String::from_str("filename"));

		// save script
		let mut script = Script::new(message.clone());

		let saved_file = match self.lua_save_script(&mut script, filename) {
			Ok(m) => m,
			Err(e) => return Err(e),
		};

		reply.insert(String::from_str("filename"), json::String(saved_file));
		return Ok(Some(reply));
	}

	pub fn lua_eval_request(&self, json: &json::Object,
			_: &RequestContext) ->
			Result<Option<json::Object>, String> {
		impl ToJson for lua::LuaValue {
			fn to_json(&self) -> json::Json {
				match self {
					&lua::Boolean(value) => json::Boolean(value),
					&lua::Number(value) => json::Number(value),
					&lua::String(ref value) => json::String(value.clone()),
					&lua::Thread => json::String(String::from_str("<thread>")),
					_ => json::String(String::from_str("<???>"))
				}
			}
		}

		let mut reply = TreeMap::new();
		reply.insert(String::from_str("type"),
			json::String(String::from_str("luaResult")));

		let message = match json_get_string(json, &String::from_str("message")) {
			Some(s) => s,
			_ => return Err(String::from_str("'message' field missing"))
		};

		let results = self.lua_eval(message);

		// change results to JSON
		let mut json_results = Vec::new();

		for result in results.iter() {
			json_results.push(result.to_json());
		}

		reply.insert(String::from_str("results"), json::List(json_results));
		return Ok(Some(reply));
	}

	pub fn lua_list_scripts_request(&self, _: &json::Object,
			_: &RequestContext) ->
			Result<Option<json::Object>, String> {
		let mut reply = TreeMap::new();

		let scripts = self.lua_list_scripts();

		let mut json_array = Vec::new();

		for s in scripts.iter() {
			json_array.push(json::String(s.clone()))
		}

		reply.insert(String::from_str("type"),
			json::String(String::from_str("listScriptsResult")));

		reply.insert(String::from_str("results"), json::List(json_array));

		return Ok(Some(reply));
	}

	pub fn srp_register(&mut self, json: &json::Object, 
			context: &mut RequestContext) -> 
			Result<Option<json::Object>, String> {
		// check auth state to see if the user's already logged/logging in
		if self.auth_state.is_some() {
			err!("user is already logged in");
		}

		// break apart the register message
		let mut reply = TreeMap::new();

		let username = try_get_param!(json, "username");
		let salt = try_get_param!(json, "salt");
		let verifier = try_get_param!(json, "verifier");

		let verifier_bigint = match 
				FromStrRadix::from_str_radix(verifier.as_slice(), 10) {
			Some(i) => i,
			_ => err!("invalid verifier")
		};

		// perform the register operation
		match context.auth_db.register(username, salt, verifier_bigint) {
			Ok(_) => {
				reply.insert(String::from_str("type"),
					json::String(String::from_str("srpRegistrationSuccess")));
			},
			Err(e) => {
				reply.insert(String::from_str("type"), 
					json::String(String::from_str("srpRegistrationFailure")));
				reply.insert(String::from_str("error"),
					json::String(e));
			}
		}

		return Ok(Some(reply));
	}

	pub fn srp_login_a(&mut self, json: &json::Object,
			context: &mut RequestContext) -> 
			Result<Option<json::Object>, String> {
		// check auth state to see if the user's already logged/logging in
		if self.auth_state.is_some() {
			err!("user is already logged in");
		}

		let mut reply = TreeMap::new();

		let username = try_get_param!(json, "username");
		let a = try_get_param!(json, "A");

		let a_bigint = match 
				FromStrRadix::from_str_radix(a.as_slice(), 10) {
			Some(i) => i,
			_ => err!("invalid A")
		};

		println!("about to begin_auth");

		match context.auth_db.begin_auth(username, a_bigint) {
			Ok(state) => {
				println!("begin_auth ok");

				reply.insert(String::from_str("type"),
					json::String(String::from_str("srpLoginB")));

				// get the contents of the srpLoginB message
				let ke = state.server_key_exchange_msg();

				// put them into the reply
				for (k, v) in ke.iter() {
					reply.insert(k.clone(), json::String(v.clone()));
				}

				self.auth_state = Some(state);
			},
			Err(e) => {
				reply.insert(String::from_str("type"),
					json::String(String::from_str("srpLoginError")));
				reply.insert(String::from_str("error"),
					json::String(e));
			}
		}

		return Ok(Some(reply));
	}

	pub fn srp_login_m(&mut self, json: &json::Object,
			_: &mut RequestContext) -> 
			Result<Option<json::Object>, String> {
		let mut reply = TreeMap::new();

		let m = try_get_param!(json, "M");

		let m_bigint = match
				FromStrRadix::from_str_radix(m.as_slice(), 10) {
			Some(i) => i,
			_ => err!("invalid M")
		};

		match self.auth_state {
			Some(ref mut state @ auth::AuthState { 
						step: auth::AwaitingClientProof, .. 
					}) => {
				println!("about to check_client_proof");

				match state.check_client_proof(m_bigint) {
					Ok(_) => {
						println!("check_client_proof ok");

						reply.insert(String::from_str("type"),
							json::String(String::from_str("srpLoginM2")));

						// get the contents of the server proof
						let proof = state.server_proof_msg().unwrap();

						// put them into the reply
						for (k, v) in proof.iter() {
							reply.insert(k.clone(), json::String(v.clone()));
						}
					}, Err(e) => {
						reply.insert(String::from_str("type"),
							json::String(String::from_str("srpLoginError")));
						reply.insert(String::from_str("error"),
							json::String(e));
					}
				}
			},
			_ => err!("received srpLoginM at wrong time")
		};

		return Ok(Some(reply));
	}

	pub fn srp_login_ok(&mut self, _: &json::Object,
			_: &mut RequestContext) -> 
			Result<Option<json::Object>, String> {
		let mut reply = TreeMap::new();

		match self.auth_state {
			Some(ref mut state @ auth::AuthState {
						step: auth::AwaitingOK, ..
					}) => {
				println!("rec'd login ok");

				match state.auth_ok() {
					Ok(_) => {
						reply.insert(String::from_str("type"),
							json::String(String::from_str("srpLoginOk")));
					},
					Err(e) => {
						reply.insert(String::from_str("type"),
							json::String(String::from_str("srpLoginError")));
						reply.insert(String::from_str("error"),
							json::String(e));
					}
				}
			},
			_ => err!("received srpLoginOK at wrong time")
		}

		return Ok(Some(reply));
	}

	pub fn srp_logout(&mut self, _: &json::Object,
			_: &mut RequestContext) ->
			Result<Option<json::Object>, String> {
		let mut reply = TreeMap::new();
		
		self.auth_state = None;

		reply.insert(String::from_str("type"),
			json::String(String::from_str("srpLogoutOk")));

		return Ok(Some(reply));
	}
}

pub struct Server {
	pub auth_db: auth::AuthDatabase,
	global_scripts: Vec<objects::Script>,
	sessions: HashMap<Uuid, Session>
}

struct RequestContext<'a> {
	auth_db: &'a mut auth::AuthDatabase,
	global_scripts: &'a mut Vec<objects::Script>
}

impl Server {
	pub fn new(auth_db_path: &str) -> Result<Server, String> {
		// load auth database
		let auth_db = match auth::AuthDatabase::load(auth_db_path) {
			Ok(db) => db,
			Err(e) => fail!(e)
		};

		let mut scripts = Vec::new();

		// load scripts
		match readdir(&Path::new("./scripts")) {
			Ok(paths) => {
				for path in paths.iter() {
					println!("loading script {}", path.as_str());
					let script = Script::load(path);

					match script {
						Ok(script) => { scripts.push(script); },
						Err(e) => err!("error loading script: {}", e)
					}
				}
			},
			Err(e) => err!("error reading script dir: {}", e)
		}

		return Ok(Server {
			sessions: HashMap::new(),
			global_scripts: scripts,
			auth_db: auth_db
		})
	}

	pub fn ensure_session(&mut self, uuid: Uuid) {
		if !self.sessions.contains_key(&uuid) {
			let session = Session::new(&self.global_scripts);
			self.sessions.insert(uuid, session);
		}
	}

	pub fn get_session<'a>(&'a mut self, uuid: Uuid) 
			-> (&'a mut Session, RequestContext<'a>) {
		(self.sessions.get_mut(&uuid),
			RequestContext {
				auth_db: &mut self.auth_db,
				global_scripts: &mut self.global_scripts
			})
	}

	pub fn autogen_script_path() -> String {
		let now = get_time();

		format!("scripts/script-{}.lua", now.sec)
	}
}

