use libc::{c_int, c_void, size_t};
use std::{c_str, ptr};

/* autogen bindings from rust-bindgen; command line: 
   LD_PRELOAD=/usr/lib/llvm-3.4/lib/libclang.so bindgen -builtins \
   -l lua5.2 -I/usr/include/lua5.2 gen.h -I /usr/lib/clang/3.4/include \
   -o c_api.rs 
*/
#[allow(dead_code)]
#[allow(non_camel_case_types)]
#[allow(unused_attribute)]
#[allow(uppercase_variables)]
mod c_api;

static lua_multret : c_int = -1;

extern fn lua_alloc(_ud: *mut c_void,
                p: *mut c_void,
                _osize: size_t,
                nsize: size_t) -> *mut c_void {
	unsafe {
        if nsize == 0 {
            // println!("freeing alloc of {} bytes", _osize);
            ::libc::free(p);
            return ptr::mut_null();
        } else {
            // println!("reallocing {} -> {} bytes", _osize, nsize);
            return ::libc::realloc(p, nsize);
        }
    }
}

pub type LuaFunction = c_api::lua_CFunction;
pub type LuaResult = c_int;

pub enum LuaValue {
	Boolean(bool),
	CClosure(LuaFunction, ::libc::c_int),
	GlobalTable,
	Integer(i32),
	Nil,
	Number(f64),
	String(String),
	Thread,
	Unsigned(u32),
	Value(::libc::c_int),
}

pub enum NResults {
	AllResults,
	SomeResults(int)
}

impl NResults {
	fn as_c_int(self) -> c_int {
		match self {
			AllResults => lua_multret,
			SomeResults(x) => x as c_int
		}
	}
}

pub struct LuaState {
	opaque: *mut c_api::lua_State
}

impl LuaState {
	pub fn new(userdata: *mut c_void) -> LuaState {
		unsafe {
			return LuaState {
				opaque: c_api::lua_newstate(Some(lua_alloc), userdata)
			};
		}
	}

	pub fn eval(&self, string: &str) -> LuaResult {
		let c_string = string.to_c_str();

		unsafe {
			let mut result = 
				c_api::luaL_loadstring(self.opaque, c_string.as_ptr());

			if result == 0 {
				result = c_api::lua_pcallk(self.opaque, 0, lua_multret, 0, 0, None);
			}

			return result;
		}
	}

	unsafe fn push(&self, value: &LuaValue) {
		match value {
			&Boolean(value) => 
				c_api::lua_pushboolean(self.opaque, if value { 1 } else { 0 }),
			&CClosure(f, n) => c_api::lua_pushcclosure(self.opaque, f, n),
			&GlobalTable => c_api::lua_rawgeti(self.opaque, -16000, 2),
			&Integer(value) => c_api::lua_pushinteger(self.opaque, value),
			&Nil => c_api::lua_pushnil(self.opaque),
			&Number(value) => c_api::lua_pushnumber(self.opaque, value),
			&String(ref value) => {
				let c_value = value.to_c_str();

				c_api::lua_pushstring(self.opaque, c_value.as_ptr());
			},
			&Thread => {
				c_api::lua_pushthread(self.opaque);

				()
			},
			&Unsigned(value) => c_api::lua_pushunsigned(self.opaque, value),
			&Value(index) => c_api::lua_pushvalue(self.opaque, index)
		}
	}

	unsafe fn pop(&self) -> Option<LuaValue> {
		let top_index = c_api::lua_gettop(self.opaque);

		let result = match c_api::lua_type(self.opaque, top_index) {
			0 /* LUA_TNIL */ => Some(Nil),
			3 /* LUA_TNUMBER */ => {
				let x = c_api::lua_tonumberx(self.opaque, top_index, 
					ptr::mut_null());

				Some(Number(x))
			},
			4 /* LUA_TSTRING */ => {
				let s = c_api::lua_tolstring(self.opaque, top_index, ptr::mut_null());
				let cstr = c_str::CString::new(s, false);

				match cstr.clone().as_str() {
					Some(s) => Some(String(String::from_str(s))),
					None => None
				}
			},
			t @ 1..8 /* not-yet-implemented */ => {
				println!("NYI: popping type {}", t);
				None
			},
			_ => None
		};

		c_api::lua_settop(self.opaque, -2);

		return result;
	}

	pub fn call<T: Vector<LuaValue>>(&self, 
			args: T, nresults: NResults) -> Vec<LuaValue> {
		unsafe {
			// subtract 1 for the function that's gonna get popped off;
			// TODO: should this be less weird?
			let top_index = c_api::lua_gettop(self.opaque) - 1;
			let args_slice = args.as_slice();

			// push arguments on the stack
			for arg in args_slice.iter() { self.push(arg); }

			// make the call
			c_api::lua_callk(self.opaque, 
				args_slice.len() as c_int, 
				nresults.as_c_int(), 
				0, 
				None);

			// pull results back off the stack
			let mut results = Vec::new();

			while top_index != c_api::lua_gettop(self.opaque) {
				match self.pop() {
					Some(x) => results.unshift(x),
					None => ()
				}
			}

			return results;
		}
	}

	pub fn pop_all(&self) -> Vec<LuaValue> {
		unsafe {
			let mut results = Vec::new();

			while c_api::lua_gettop(self.opaque) != 0 {
				match self.pop() {
					Some(x) => results.unshift(x),
					None => ()
				}
			}

			return results;
		}
	}

	pub fn getglobal(&self, name: &str) {
		let c_name = name.to_c_str();

		unsafe {
			c_api::lua_getglobal(self.opaque, c_name.as_ptr());
		}
	}
}