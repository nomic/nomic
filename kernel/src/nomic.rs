#![feature(macro_rules)]

extern crate collections;
extern crate libc;
extern crate rabbitmq;
extern crate serialize;
extern crate uuid;
extern crate num;
extern crate time;

use serialize::json;
use serialize::json::ToJson;
use std::string::String;
use uuid::Uuid;
use server::Server;

mod lua;
mod auth;
mod objects;
mod server;

fn json_get_string(x: &json::Object, k: &String) -> Option<String> {
	match x.find(k) {
		Some(&json::String(ref s)) => { return Some(s.clone()); }
		_ => { return None; }
	}
}

fn reply_to_message(msg: rabbitmq::Message,
		server: &mut Server,
		con: &rabbitmq::Connection,
		chan: &rabbitmq::Channel) {
	let properties = rabbitmq::amqp_basic_properties {
		_flags: (1 << 15),
		content_type: "text/plain".to_string(),
		delivery_mode: 1,
		..std::default::Default::default()
	};

	match json::from_str(msg.str_body().unwrap()) {
		Ok(json::Object(x)) => {
			println!("json parsing");

			let reply_to = match json_get_string(&x, &String::from_str("reply-to")) {
				Some(re) => re,
				_ => return
			};

			let m_type = match json_get_string(&x, &String::from_str("type")) {
				Some(t) => t,
				_ => return
			};

			let session_id = match json_get_string(&x, 
					&String::from_str("id")) {
				Some(id) => {
					match from_str::<Uuid>(id.as_slice()) {
						Some(uuid) => uuid,
						None => return
					}
				},
				None => return
			};

			server.ensure_session(session_id);

			let (session, mut context) = server.get_session(session_id);

			let result = match m_type.as_slice() {
				"lua" => session.lua_save_eval(&x, &mut context),
				"luaSave" =>
					session.lua_save(&x, &context),
				"listScripts" =>
					session.list_scripts_request(&x, &context),
				"srpRegistration" => 
					session.srp_register(&x, &mut context),
				"srpLoginA" => 
					session.srp_login_a(&x, &mut context),
				"srpLoginM" => 
					session.srp_login_m(&x, &mut context),
				"srpLoginOk" => 
					session.srp_login_ok(&x, &mut context),
				"srpLogout" =>
					session.srp_logout(&x, &mut context),
				_ => Err(String::from_str("unknown message type"))
			};

			match result {
				// perform type-independent processing: put ID back in, send reply
				Ok(Some(mut reply)) => {
					match json_get_string(&x, &String::from_str("id")) {
						Some(id) => {
							println!("id is {}", id);
							reply.insert(String::from_str("id"), json::String(id));
						},
						_ => {}
					}

					println!("sending {}", reply);
					(*con).basic_publish(*chan, "", reply_to.as_slice(), false,
						false, Some(properties), 
						reply.to_json().to_string().into_bytes());

					return
				},
				_ => {
					return
				}
			}

		},
		Ok(_) => {
			println!("unexpected json: {}", msg.str_body());
		},
		Err(err) => {
			println!("error parsing json: {}", err);
		}
	}	
}

fn main() {
	let mut con = rabbitmq::Connection::new(rabbitmq::TcpSocket).unwrap();

	let mut server = match Server::new("passwd") {
		Ok(srv) => srv,
		Err(e) => fail!(e)
	};

	let result = con.socket_open("localhost", None);

	if result.is_err() {
		println!("error");
	} else {
		println!("connected to mq");
	}

	match con.login("/", 0, None, 0, rabbitmq::AMQP_SASL_METHOD_PLAIN, 
			"guest", "guest") {
		Err(err) => { fail!(err); },
		_ => {}
	}

	let chan = con.channel_open(1).unwrap();

	match con.queue_declare(chan, "kernel", false, false, false, 
			false, None) {
		Err(err) => { fail!(err); },
		_ => {}
	}

	con.basic_consume(chan, "", "kernel", false, false, false, None);

	'read_loop: loop {
		match con.consume_message(None, None) {
			Ok(envelope) => {
				let msg = envelope.message;

				println!("message: {}", msg.str_body());
				
				reply_to_message(msg, &mut server, &con, &chan);
				con.ack(chan, envelope.delivery_tag, false);
			},
			Err(msg) => {
				println!("error: {}", msg);
				break 'read_loop;
			}
		}
	}

	con.channel_close(chan, rabbitmq::AMQP_REPLY_SUCCESS);
	con.connection_close(rabbitmq::AMQP_REPLY_SUCCESS);
}
