/* automatically generated by rust-bindgen */

pub type __builtin_va_list = *mut ::libc::c_char;
#[link(name = "nacl")]
extern "C" {
    pub fn crypto_hash_sha256_ref(arg1: *mut ::libc::c_uchar,
                                  arg2: *const ::libc::c_uchar,
                                  arg3: ::libc::c_ulonglong) -> ::libc::c_int;
    pub fn crypto_hash_sha512_ref(arg1: *mut ::libc::c_uchar,
                                  arg2: *const ::libc::c_uchar,
                                  arg3: ::libc::c_ulonglong) -> ::libc::c_int;
}
