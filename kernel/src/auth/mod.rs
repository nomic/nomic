extern crate core;
extern crate num;

use self::core::num::{Zero};
use self::num::bigint::{BigUint, ToBigUint};
use serialize::hex::{FromHex};
use std::collections::HashMap;
use std::io::{BufferedReader, File, Open, Append, Read, Write};
use std::num::{FromStrRadix, ToStrRadix};

/* autogen bindings from rust-bindgen; command line: 
   LD_PRELOAD=/usr/lib/llvm-3.4/lib/libclang.so bindgen -builtins \
   -l nacl -I/usr/include/nacl nacl_gen.h -I /usr/lib/clang/3.4/include \
   -o nacl_api.rs 
*/
#[allow(dead_code)]
#[allow(non_camel_case_types)]
#[allow(unused_attribute)]
mod nacl_api; 

mod srp;

struct AuthDatabaseUser {
	username: String,
	salt: Vec<u8>,
	verifier: BigUint
}

impl AuthDatabaseUser {
	pub fn from_db_rec(record: String) -> 
			Result<AuthDatabaseUser, String> {
		let fields: Vec<&str> = record.as_slice().trim().
			split(':').collect();

		let salt: Vec<u8> = match fields.get(1).from_hex() {
			Ok(vec) => vec,
			Err(e) => fail!("error parsing salt {}: {}", fields.get(1), e)
		};

		let verifier = match FromStrRadix::from_str_radix(*fields.get(2), 16) {
			Some(i) => i,
			None => fail!("error parsing verifier {}", fields.get(2))
		};

		return Ok(AuthDatabaseUser { 
			username: String::from_str(*fields.get(0)),
			salt: salt,
			verifier: verifier
		})
	}

	#[allow(deprecated)]
	pub fn to_db_rec(&self) -> String {
		// smash salt bytes back together into a bigint
		let salt_bigint = self.salt.iter().
			fold(Zero::zero(), 
				 |a: BigUint, b: &u8| {
			 		(a << 8) + b.to_biguint().unwrap()
			 	 });
		let salt_string = salt_bigint.to_str_radix(16);

		return format!("{}:{}:{}", 
			self.username, 
			salt_string,
			self.verifier.to_str_radix(16));
	}
}

#[deriving(Show)]
pub enum AuthStateStep {
	AwaitingClientProof,
	AwaitingOK,
	Finished,
	Failed
}

pub struct AuthState {
	opaque: srp::SRPAuthState,
	pub step: AuthStateStep,
	server_proof: Option<BigUint>
}

impl AuthState {
	#[allow(uppercase_variables)]
	pub fn check_client_proof(&mut self, M: BigUint) -> 
			Result<(), String> {
		match self.step {
			AwaitingClientProof => {},
			_ => fail!("check_client_proof: wrong step {}", self.step)
		};

		let result = self.opaque.prove(M);

		match result {
			Ok(m2) => {
				self.server_proof = Some(m2);
				self.step = AwaitingOK;
				return Ok(());
			},
			Err(e) => {
				self.step = Failed;
				return Err(e);
			}
		}
	}

	pub fn auth_ok(&mut self) -> Result<(), String> {
		match self.step {
			AwaitingOK => {},
			_ => fail!("auth_ok: wrong step {}", self.step)
		}

		self.step = Finished;
		return Ok(());
	}

	#[allow(deprecated)]
	pub fn server_key_exchange_msg(&self) -> HashMap<String, String> {
		match self.step {
			Failed => fail!("auth state failed"),
			_ => {}
		}

		let mut msg = HashMap::new();

		// turn salt back into a string
		let salt_bigint = self.opaque.salt.iter().
			fold(Zero::zero(), 
				 |a: BigUint, b: &u8| {
			 		(a << 8) + b.to_biguint().unwrap()
			 	 });
		let salt_string = salt_bigint.to_str_radix(16);

		msg.insert(String::from_str("salt"), salt_string);

		// insert B
		msg.insert(String::from_str("B"), self.opaque.B.to_str_radix(16));

		return msg;
	}

	#[allow(deprecated)]
	pub fn server_proof_msg(&self) 
			-> Result<HashMap<String, String>, String> {
		match self.step {
			Failed => fail!("auth state failed"),
			_ => {}
		}

		let mut msg = HashMap::new();

		// add server proof
		match self.server_proof {
			Some(ref m2) => {
				msg.insert(String::from_str("M2"), m2.to_str_radix(16));
			},
			None => {
				fail!(String::from_str("server proof not computed yet"));
			}
		}

		return Ok(msg);
	}

	pub fn username(&self) -> String {
		return self.opaque.username.clone();
	}
}

pub struct AuthDatabase {
	file: File,
	users: HashMap<String, AuthDatabaseUser>
}

impl AuthDatabase {
	pub fn load(path: &str) -> Result<AuthDatabase, String> {
		let p = Path::new(path);
		let mut users = HashMap::new();

		if p.exists() {
			let file = match File::open_mode(&p, Open, Read) {
				Ok(f) => f,
				Err(e) => return Err(format!("error opening database: {}", e))
			};
	
			let mut reader = BufferedReader::new(file);
			for rec in reader.lines() {
				match rec {
					Ok(s) => {
						let user = try!(AuthDatabaseUser::from_db_rec(s));

						users.insert(user.username.clone(), user);
					}
					Err(e) => return Err(format!("error while reading: {}", e))
				}
			}
		}

		match File::open_mode(&p, Append, Write) {
			Ok(f) => return Ok(AuthDatabase { file: f, users: users }),
			Err(e) => return Err(format!("error reopening database: {}", e))
		}
	}

	pub fn register(&mut self, username: String, salt: String, verifier: BigUint) 
			-> Result<(), String> {
		if self.users.contains_key(&username) {
			return Err(String::from_str("user already exists"))
		}

		let salt_vec = match salt.as_slice().from_hex() {
			Ok(vec) => vec,
			Err(e) => fail!("failed to parse salt {}: {}", salt, e)
		};

		let new_user = AuthDatabaseUser {
			username: username.clone(),
			salt: salt_vec,
			verifier: verifier
		};

		match self.file.write_line(new_user.to_db_rec().as_slice()) {
			Ok(_) => {
				self.users.insert(username, new_user);
				return Ok(());
			},
			Err(_) => 
				return Err(
					String::from_str("error saving new user to database")
				)
		}
	}

	#[allow(uppercase_variables)]
	pub fn begin_auth(&self, username: String, A: BigUint) ->
			Result<AuthState, String> {
		let user = match self.users.find(&username) {
			Some(u) => u,
			None => return Err(String::from_str("no such user"))
		};

		let group = srp::Group::group_2048();

		println!("about to create srp state");

		let srp_state = try!(srp::SRPAuthState::new(group, 
			username, user.salt.clone(), user.verifier.clone(), A));

		println!("srp state done");

		return Ok(AuthState {
			opaque: srp_state,
			step: AwaitingClientProof,
			server_proof: None
		});
	}
}
