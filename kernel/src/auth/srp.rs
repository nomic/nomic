// SRP parameters are named identically to those in the spec for clarity,
// and unfortunately cryptographers like upper-case variable names
#![allow(uppercase_variables)]

// to_str_radix() is deprecated but there's no radix formatter for 
// BigUint yet
#![allow(deprecated)]

extern crate core;
extern crate num;
extern crate rand;

use super::nacl_api;

use self::core::num::{Zero, One};
use self::num::bigint::{BigUint, ToBigUint};
use num::bigint::RandBigInt;
use num::Integer;
use self::rand::SeedableRng;
use self::rand::isaac::IsaacRng;
use std::num::{FromStrRadix};
use std::path::BytesContainer;
use std::rand::random;

pub struct Group {
	prime: BigUint,
	generator: BigUint
}

impl Group {
	fn new(hex_prime: &str, generator: u32) -> Group {
		let mut prime_str = String::from_str(hex_prime);

		prime_str = prime_str.replace(" ", "");
		prime_str = prime_str.replace("\t", "");
		prime_str = prime_str.replace("\r", "");
		prime_str = prime_str.replace("\n", "");

		// convert prime to a bigint
		let prime_bigint = 
			FromStrRadix::from_str_radix(prime_str.as_slice(), 16);

		// spit out a group
		return Group {
			prime: prime_bigint.unwrap(),
			generator: BigUint::from_slice([generator])
		}
	}
}

#[test]
fn test_group() {
	let prime = "11
	22
	33
	44";
	let generator = 3;

	let g = Group::new(prime, generator);

	if g.prime != BigUint::from_slice([0x11223344]) {
		fail!("expected prime = 0x11223344, got {}", g.prime);
	}

	if g.generator != BigUint::from_slice([3]) {
		fail!("expected g = 3, got {}", g.generator);
	}
}

pub struct SRPAuthState {
	// algorithm parameters
	group: Group,
	k: BigUint,

	// persistent authentication data
	pub username: String,
	pub salt: Vec<u8>,
	verifier: BigUint,
	
	// ephemeral authentication data
	A: BigUint,
	pub B: BigUint,
	b: BigUint,
	K: BigUint
}

impl SRPAuthState {
	pub fn new(group: Group, username: String, salt: Vec<u8>, 
			verifier: BigUint, A: BigUint) -> Result<SRPAuthState, String> {
		if (A % group.prime) == Zero::zero() {
			fail!("insecure parameters: A == 0 (mod N)");
		}

		let k = SRPAuthState::k(&group);

		// compute B
		let (b, B) = SRPAuthState::choose_b(&k, &verifier, &group);

		// compute u
		let A_bytes = biguint_to_bytes(&A).pad(&group);
		let B_bytes = biguint_to_bytes(&B).pad(&group);
		let u = hash(&(A_bytes + B_bytes));

		// compute S
		let S = modular_exp(
			&((A * modular_exp(&verifier, &u, &group.prime)) % group.prime),
			&b, &group.prime);
		let K = hash_biguint(&S);

		return Ok(SRPAuthState {
			group: group, k: k,
			username: username, salt: salt, verifier: verifier,
			A: A, B: B, b: b, K: K
		});
	}

	// submit the client proof of K, and respond with either the server proof
	// or an error
	pub fn prove(&self, M: BigUint) -> Result<BigUint, String> {
		// check the client's proof against what we expect H(A, B, K) to be
		let A_bytes = biguint_to_bytes(&self.A).pad(&self.group);
		let B_bytes = biguint_to_bytes(&self.B).pad(&self.group);
		let K_bytes = biguint_to_bytes(&self.K).pad(&self.group);

		let expect_M = hash(&(A_bytes + B_bytes + K_bytes));

		// if the proofs don't match, bail out now
		if expect_M != M {
			return Err(String::from_str("client proof is invalid"));
		}

		// now compute our proof for the client
		let M_bytes = biguint_to_bytes(&expect_M).pad(&self.group);

		let server_proof =
			hash(&(A_bytes + M_bytes + K_bytes));

		return Ok(server_proof);
	}

	fn choose_b(k: &BigUint, verifier: &BigUint, group: &Group) -> 
			(BigUint, BigUint) {
		let mut B: BigUint = Zero::zero();
		let mut b: BigUint = Zero::zero();

		let seed: &[u32] = [random()];
		let mut rng: IsaacRng = SeedableRng::from_seed(seed);

		while B == Zero::zero() {
			b = rng.gen_biguint(256u);
			B = (k * (*verifier) + 
				 modular_exp(&group.generator, &b, &group.prime)) 
				% group.prime;
		}

		return (b, B);
	}

	// compute k = H(N, g) for the given group
	fn k(group: &Group) -> BigUint {
		let N_bytes = biguint_to_bytes(&group.prime);
		let g_bytes = biguint_to_bytes(&group.generator).pad(group);

		let to_hash = N_bytes + g_bytes;

		return hash(&to_hash);
	}
}

fn biguint_to_bytes(x: &BigUint) -> Vec<u8> {
	let mut bytes = Vec::new();
	let mut remainder = x.clone();
	let divisor = 256u16.to_biguint().unwrap();

	while remainder > Zero::zero() {
		bytes.push((remainder % divisor).to_u8().unwrap());
		remainder = remainder >> 8;
	}

	bytes.reverse();

	return bytes;
}

/* friendlier hash methods */
fn hash(x: &Vec<u8>) -> BigUint {
	let bytes = x.container_as_bytes();
	let mut hash = Vec::from_elem(32, 0u8);

	unsafe {
		nacl_api::crypto_hash_sha256_ref(hash.as_mut_ptr(),
			bytes.as_ptr(), x.len() as u64);
	}

	// smoosh the hash bytes back together into a bigint
	let h_bigint = hash.iter().
		fold(Zero::zero(), 
			 |a: BigUint, b: &u8| {
			 	(a << 8) + b.to_biguint().unwrap()
			 });

	return h_bigint;
}

fn hash_biguint(n: &BigUint) -> BigUint {
	return hash(&biguint_to_bytes(n));
}

/* biguint modular exponentiation: (b^e) mod N */
fn modular_exp(b: &BigUint, e: &BigUint, N: &BigUint) -> BigUint {
	let mut result: BigUint = One::one();
	let mut base = b.clone() % *N;
	let mut exponent = e.clone();

	while exponent > Zero::zero() {
		if exponent.is_odd() {
			result = (result * base) % *N;
		}

		exponent = exponent >> 1;
		base = (base * base) % *N;
	}

	return result;
}

#[test]
/* test vector per http://en.wikipedia.org/wiki/Modular_exponentiation,
   whence I also stole the modular_exp() code */
fn test_modular_exp() {
	let b: BigUint = BigUint::from_slice([4]);
	let e: BigUint = BigUint::from_slice([13]);
	let N: BigUint = BigUint::from_slice([497]);

	let result = modular_exp(&b, &e, &N);

	if result != BigUint::from_slice([445]) {
		fail!("expected 445, got {}", result);
	}
}

trait Pad {
	/* as per the PAD() function in RFC 5054 */
	fn pad(&self, group: &Group) -> Self;
}

impl Pad for Vec<u8> {
	fn pad(&self, group: &Group) -> Vec<u8> {
		// compute the length of the group prime in bytes
		let bits = group.prime.bits();
		let bytes = ((bits as f32) / 8.0).ceil() as uint;

		if self.len() > bytes {
			fail!("unpadded string is wider ({}) than group prime ({})",
				self.len(), bytes);
		}

		let padding_len = bytes - self.len();
		let padding = Vec::from_elem(padding_len, 0);

		return padding + *self;
	}
}

/* magic numbers from RFC 5054 appendix A 
   (http://tools.ietf.org/html/rfc5054#appendix-A) */
#[allow(dead_code)]
impl Group {
	pub fn group_1024() -> Group {
		return Group::new(
			"EEAF0AB9 ADB38DD6 9C33F80A FA8FC5E8 60726187 75FF3C0B 9EA2314C
			9C256576 D674DF74 96EA81D3 383B4813 D692C6E0 E0D5D8E2 50B98BE4
			8E495C1D 6089DAD1 5DC7D7B4 6154D6B6 CE8EF4AD 69B15D49 82559B29
			7BCF1885 C529F566 660E57EC 68EDBC3C 05726CC0 2FD4CBF4 976EAA9A
			FD5138FE 8376435B 9FC61D2F C0EB06E3",
			2
			);
	}

	pub fn group_1536() -> Group {
		return Group::new(
			"9DEF3CAF B939277A B1F12A86 17A47BBB DBA51DF4 99AC4C80 BEEEA961
			4B19CC4D 5F4F5F55 6E27CBDE 51C6A94B E4607A29 1558903B A0D0F843
			80B655BB 9A22E8DC DF028A7C EC67F0D0 8134B1C8 B9798914 9B609E0B
			E3BAB63D 47548381 DBC5B1FC 764E3F4B 53DD9DA1 158BFD3E 2B9C8CF5
			6EDF0195 39349627 DB2FD53D 24B7C486 65772E43 7D6C7F8C E442734A
			F7CCB7AE 837C264A E3A9BEB8 7F8A2FE9 B8B5292E 5A021FFF 5E91479E
			8CE7A28C 2442C6F3 15180F93 499A234D CF76E3FE D135F9BB",
			2
			);
	}

	pub fn group_2048() -> Group {
		return Group::new(
			"AC6BDB41 324A9A9B F166DE5E 1389582F AF72B665 1987EE07 FC319294
			3DB56050 A37329CB B4A099ED 8193E075 7767A13D D52312AB 4B03310D
			CD7F48A9 DA04FD50 E8083969 EDB767B0 CF609517 9A163AB3 661A05FB
			D5FAAAE8 2918A996 2F0B93B8 55F97993 EC975EEA A80D740A DBF4FF74
			7359D041 D5C33EA7 1D281E44 6B14773B CA97B43A 23FB8016 76BD207A
			436C6481 F1D2B907 8717461A 5B9D32E6 88F87748 544523B5 24B0D57D
			5EA77A27 75D2ECFA 032CFBDB F52FB378 61602790 04E57AE6 AF874E73
			03CE5329 9CCC041C 7BC308D8 2A5698F3 A8D0C382 71AE35F8 E9DBFBB6
			94B5C803 D89F7AE4 35DE236D 525F5475 9B65E372 FCD68EF2 0FA7111F
			9E4AFF73",
			2
			);
	}
}
