use std::io::{File, Truncate, Write};
use super::super::lua::{LuaState, LuaResult};

macro_rules! err(
	($msg:expr $(, $arg:tt)*) => (
		return Err(format!($msg $(, $arg)*))
	)
)

#[deriving(Clone)]
pub struct Script {
	pub path: Option<Path>,
	pub text: String
}

impl Script {
	pub fn new(text: String) -> Script {
		Script {
			path: None,
			text: text
		}
	}

	pub fn load(path: &Path) -> Result<Script, String> {
		let mut f = match File::open(path) {
			Ok(f) => f,
			Err(e) => err!("error opening script: {}", e)
		};

		match f.read_to_string() {
			Ok(s) => {
				let script = Script {
					path: Some(path.clone()),
					text: s
				};

				return Ok(script)
			},
			Err(e) => err!("error reading script: {}", e)
		};
	}

	pub fn save(&mut self, new_path: Option<&Path>) 
			-> Result<(), String> {
		let save_path = match (new_path, &self.path) {
			(Some(&ref s), _) => {
				Path::new(s)
			},
			(None, &Some(ref p)) => p.clone(),
			_ => err!("no path set for file")
		};

		let mut f = match File::open_mode(&save_path, 
				Truncate, Write) {
			Ok(f) => f,
			Err(e) => err!("error opening file to save: {}", e)
		};

		match f.write_str(self.text.as_slice()) {
			Ok(_) => {
				self.path = Some(save_path);

				return Ok(())
			},
			Err(e) => err!("error writing script: {}", e)
		}
	}

	pub fn load_into(&self, lua: &LuaState) -> LuaResult {
		lua.eval(self.text.as_slice())
	}
}
