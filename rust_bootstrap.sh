#!/bin/bash
###########################################
# rust stuff: build things from source #
###########################################

TARBALL_DIR=rust-nightly-i686-unknown-linux-gnu
TARBALL_NAME=rust-nightly-i686-unknown-linux-gnu.tar.gz
RUST_DISTRO_PATH=http://static.rust-lang.org/dist

cd ~

echo "HOME=$HOME"

# install rust from binaries
wget --quiet $RUST_DISTRO_PATH/$TARBALL_NAME
tar zxf $TARBALL_NAME

pushd $TARBALL_DIR
./install.sh --prefix=$HOME

popd
rm -rf ~/$TARBALL_DIR ~/$TARBALL_NAME

# add ~/bin to path
export PATH=$PATH:~/bin

# make it permanent
cat >> ~/.bash_profile <<END
export PATH=$PATH:~/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/lib
END

# install cargo from source
git clone --recursive https://github.com/rust-lang/cargo

pushd cargo

make
PREFIX=$HOME make install

popd
rm -rf ~/cargo

# download rust binding generator
git clone https://github.com/crabtw/rust-bindgen

pushd rust-bindgen

# per rust-bindgen/README.md, modified slightly to account for Ubuntu
# library install paths
rustc -L /usr/lib/llvm-3.4/lib lib.rs
rustc -L . -L /usr/lib/llvm-3.4/lib bindgen.rs
cp ./bindgen ~/bin
cp ./libbindgen* ~/lib

popd
rm -rf ~/rust-bindgen

# build the nomic rust kernel
pushd /vagrant/kernel
cargo build -u
popd
