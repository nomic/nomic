#!/bin/bash

###########################################
# root stuff: install stuff from packages #
###########################################

# git, because everything requires git
apt-get install -y git

# install python
apt-get install -y python python-dev python-pip

# install lib dependencies
apt-get install -y rabbitmq-server librabbitmq-dev
apt-get install -y liblua5.2-dev
apt-get install -y libnacl-dev # NaCl for crypto
apt-get install -y libclang-dev # required by lua binding generator

# install web dependencies
pushd /vagrant/web

pip install -r requirements.txt --upgrade

popd

###########################################
# vagrant stuff: build things from source #
###########################################

su - vagrant -c'source /vagrant/rust_bootstrap.sh'

###########################################
# install & start nomic services          #
###########################################
pushd /vagrant/web
mkdir -p log
popd

pushd /vagrant/config
cp nomic-kernel.conf /etc/init/
cp nomic-web.conf /etc/init/
popd

pushd /vagrant/util
cp nomic-build-kernel.sh /usr/local/bin/nomic-build-kernel
cp nomic-restart.sh /usr/local/bin/nomic-restart
popd

echo "Starting nomic-kernel"
service nomic-kernel start

echo "Starting nomic-web"
service nomic-web start

########################
# root stuff: clean up #
########################

# clean up unused packages
apt-get autoremove
