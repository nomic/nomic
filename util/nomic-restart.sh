#!/bin/bash

echo "Stopping Nomic"
sudo service nomic-web stop
sudo service nomic-kernel stop
echo "Starting Nomic"
sudo service nomic-kernel start
sudo service nomic-web start
