from gevent import monkey
monkey.patch_all()

from flask import Flask, render_template, request, \
	copy_current_request_context, redirect, current_app
from flask.ext.socketio import SocketIO, emit
from gevent import Greenlet
from flask.ext.assets import Environment, Bundle

from uuid import uuid4
from json import dumps as to_json

from kernel import send_to_kernel, amqp_handler

import logging
import pdb
import importlib

app = Flask(__name__)
app.debug = True

MODULES = [
	'auth',
	'devenv',
	'register',
	'dev'
]

DEFAULT_MODULE = 'devenv'

BASE_CSS = [
	'fonts/munro/stylesheet.css',
	'fonts/ubuntu/stylesheet.css',
	'scss/base.scss'
]

# Register blueprint(s) & bundle assets for each.
def initialize_modules():

	assets     = Environment(app)
	assets.url = app.static_url_path

	css_base   = Bundle(*BASE_CSS, filters='pyscss', output='css/_compiled/css_base.css')
	assets.register('css_base', css_base)

	for module_name in MODULES:

		import imp
		module = imp.load_source('', '%s/controllers/%s.py' % (app.root_path, module_name))
		app.register_blueprint(getattr(module, 'mod_%s' % module_name))

		scss_files = getattr(module, 'scss_files')
		scss_files.append('scss/%s.scss' % module_name)
		m_assets = Bundle(*scss_files, filters='pyscss', output='css/_compiled/css_%s.css' % module_name)
		assets.register('css_%s' % module_name, m_assets)


def initialize_logger():
	# Set up logger
	file_handler = logging.FileHandler(app.root_path + '/log/web.log', mode='a+')
	file_handler.setLevel(logging.DEBUG)
	app.logger.addHandler(file_handler)

socketio = SocketIO(app)

requests = dict()

@app.route('/')
def index():
	return redirect('/%s' % DEFAULT_MODULE)

@app.route('/kernel', methods=['POST'])
def flask_to_kernel():
	postData = request.form
	msgData = dict(zip(postData.keys(), map(str, postData.values())))
	send_to_kernel(dict(msgData))
	return to_json({})

@socketio.on('kernel')
def socketio_to_kernel(data):
	send_to_kernel(data)

@socketio.on('join')
def on_join(data):
	request_uuid = uuid4()
	current_app.logger.debug("Stashing request context for {0}".format(request_uuid))

	requests[str(request_uuid)] = {
		'request': request,
		'emit': copy_current_request_context(emit)
	}

	# Tell the client what UUID we gave them
	registration_data = { 'uuid' : str(request_uuid) }
	emit('registered', registration_data)
	current_app.logger.debug("Emitting 'Registered' - {0}".format(to_json(registration_data)))

	join_msg = "{0} joined.".format(data['username'])
	current_app.logger.info(join_msg)

if __name__ == "__main__":
	initialize_logger()
	initialize_modules()
	g = Greenlet.spawn(amqp_handler, requests, app.logger)
	socketio.run(app, host='0.0.0.0')
