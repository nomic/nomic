import web
import unittest

class NomicTestCase(unittest.TestCase):

  def setUp(self):
    web.app.config['TESTING'] = True
    self.app = web.app.test_client()

  def tearDown(self):
    pass

  def test_index_redirect(self):
    rv = self.app.get('/')
    self.assertEqual(302, rv.status_code)

  def test_get_login(self):
    rv = self.app.get('/login')
    self.assertEqual(200, rv.status_code)

if __name__ == '__main__':
    unittest.main()
