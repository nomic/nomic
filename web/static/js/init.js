define(['component/data/socket_client',
        'component/ui/editor',
        'component/ui/terminal',
        'component/ui/register',
        'component/ui/login',
        'component/ui/header_button',
        'component/data/storage_client',

        // Interface only
        'lib/jquery.min'],

function(SocketData,
         ComposeBox,
         Terminal,
         Register,
         Login,
         HeaderButton,
         StorageClient)
{

  'use strict';

  SocketData.attachTo(document);
  ComposeBox.attachTo('#echo-message');
  Terminal.attachTo('#echo-output');
  Register.attachTo('#register-form');
  Login.attachTo('#login-form');
  HeaderButton.attachTo('.save-button');
  StorageClient.attachTo(document);

  // window.onbeforeunload = function(e) {
  //   console.log("onBeforeUnload() handler called");

  //   var sendBtn = document.getElementById('echo-send-btn');
  //   sendBtn.disabled = true;

  //   if (socket) {
  //     socket.disconnect();
  //   }
  //   console.log("disconnected");
  // }
});

