define(['lib/jquery.min', 'lib/flight'],
function(){
  'use strict';

  return flight.component(headerButton);

  function headerButton(){

    var action;
    var icon;

    this.handleClick = function(e) {
      this.trigger('uiSaveTriggered');
    };

    this.doAnimation = function(e) {
      this.$node.addClass('clicked');
    };

    this.handleAnimationEnd = function(e){
      this.$node.removeClass('clicked');
    };

    this.after('initialize', function(){
      action = this.$node.attr('data-action');
      icon   = this.$node.find('i')[0];

      this.on('webkitAnimationEnd', this.handleAnimationEnd.bind(this));
      this.on('click', this.handleClick);
      this.on(document, 'uiSaveTriggered', this.doAnimation.bind(this));
    });
  }
});