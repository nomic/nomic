define(['lib/jsbn',
        'lib/sha1',
        'lib/sjcl',
        'lib/srp-client',
        'lib/jquery.min',
        'lib/flight'],

function() {

  return flight.component(login);

  function login()
  {
    var hashFunc = 'sha-256';
    var group = 2048;
    var uuid;
    var user;
    var srp;
    var srp_a, srp_A, srp_S, srp_K, srp_M;

    this.saveUUID = function(e, data)
    {
      uuid = data.uuid;
    }

    this.loginSRP_A = function(username, password)
    {
      //TODO: Display login progress somewhere
      $('#login-btn').attr("disabled", true);
      $('#username').attr("disabled", true);
      $('#password').attr("disabled", true);

      user = username;
      srp = new SRPClient(username, password, group, hashFunc);

      srp_a = srp.srpRandom();
      srp_A = srp.calculateA(srp_a);

      console.log("Sending srpLoginA to server");
      showProgress("Uploading DNA verification matrix. Do not look directly at laser. Please standby...");

      $.post('/kernel', {
          'type': 'srpLoginA',
          'username': user,
          'A': srp_A.toString(),
          'id': uuid,
      });
    };

    this.loginSRP_B = function(e, data)
    {
      console.log("Received loginSRP_B from server");
      var srp_B = new BigInteger(data['B'], 16);
      var salt = new BigInteger(data['salt'], 16);

      showProgress("Reticulating splines to server specified frequency.");

      if(srp_B.toString(16) === "0")
      {
        console.log("Aborted SRP Login - Server sent B == 0");
        showError('Error negotiating login with server');

        $.post('/kernel', {
          'type': 'srpLoginError',
          'error': 'aborted. b was equal to 0',
          'username': user,
          'id': uuid,
        });

        $.post('/kernel', {
          'type': 'srpLogout',
          'username': user,
          'id': uuid,
        });

        return;
      }

      var srp_u = srp.calculateU(srp_A, srp_B);

      if(srp_u.toString(16) === "0")
      {
        console.log("Aborted SRP Login - u == 0");
        showError('Error negotiating login with server');

        $.post('/kernel', {
          'type': 'srpLoginError',
          'error': 'aborted. u was equal to 0',
          'username': user,
          'id': uuid,
        });

        $.post('/kernel', {
          'type': 'srpLogout',
          'username': user,
          'id': uuid,
        });

        return;
      }

      srp_S = srp.calculateS(srp_B, salt.toString(16), srp_u, srp_a);

      srp_K = srp.calculateK(srp_S);
      srp_M = srp.calculateM(srp_A, srp_B, srp_K);
      console.log("Sending srpLoginM to server");

      $.post('/kernel', {
          'type': 'srpLoginM',
          'username': user,
          'M': srp_M.toString(),
          'id': uuid,
      });
    };

    this.loginSRP_M2 = function(e, data)
    {
      console.log("Received srpLoginM2 from server")

      var clientM2 = srp.calculateM(srp_A, srp_M, srp_K);
      var serverM2 = new BigInteger(data['M2'], 16);

      showProgress("Verifying client implant matches monitoring station logs.");

      if(clientM2.toString(16) === serverM2.toString(16))
      {
        console.log("Client and server secrets match. Sick");
        console.log("Sending srpLoginOK to server");
        $.post('/kernel', {
          'type': 'srpLoginOk',
          'username': user,
          'id': uuid,
        });
      } else {
        console.log("Client and server secrets do not match.");
        console.log("Client M2: "+ clientM2.toString(16));
        console.log("Server M2: "+ serverM2.toString(16));
        showError('Error negotiating login with server.');

        $.post('/kernel', {
          'type': 'srpLoginError',
          'error': 'Client M2 and server M2 do not match. Client: '+ clientM2.toString(16) +' Server: '+ M2.toString(16),
          'username': user,
          'id': uuid,
        });

        $.post('/kernel', {
          'type': 'srpLogout',
          'username': user,
          'id': uuid,
        });
      }
    };

    this.loginError = function(e, data)
    {
      console.log("SRP Login failure. Error message: "+ data['error']);
      showError('Error logging in: '+ data['error']);

      $.post('/kernel', {
        'type': 'srpLogout',
        'username': user,
        'id': uuid,
      });
    };

    this.logoutSuccess = function(e, data)
    {
      console.log("Successfully logged out & reset srp auth state");
      $('#login-btn').attr("disabled", false);
      $('#username').attr("disabled", false);
      $('#password').attr("disabled", false);
    };

    this.loginSuccess = function(e, data)
    {
      //TODO: Do more than just redirect silently to / (a page that currently
      //      requires no auth anyway...
      document.location = '/';
    };

    function showError(message)
    {
      $('#login-progress-container').show();

      $('#login-progress').hide();
      $('#login-progress-message').hide();

      $('#login-error').show();
      $('#login-error-message').show();
      $('#login-error-message').text(message);
    };

    function showProgress(message)
    {
      $('#login-progress-container').show();

      $('#login-error').hide();
      $('#login-error-message').hide();

      $('#login-progress').show();
      $('#login-progress-message').show();
      $('#login-progress-message').text(message);
    };

    this.after('initialize', function(){
      this.on(document, 'srpLoginB', this.loginSRP_B.bind(self));
      this.on(document, 'srpLoginM2', this.loginSRP_M2.bind(self));
      this.on(document, 'srpLoginError', this.loginError.bind(self));
      this.on(document, 'srpLoginOk', this.loginSuccess.bind(self));
      this.on(document, 'srpLogoutOk', this.logoutSuccess.bind(self));
      this.on(document, 'dataRegistered', this.saveUUID.bind(self));

      var self = this;

      $('#login-form').submit(function(e) {
        e.preventDefault();
        var username = $("#username").val();
        var password = $("#password").val();
        self.loginSRP_A(username, password);
      });
    });
  };
});
