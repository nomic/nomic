define(['lib/jquery.min', 'lib/flight'],
function(){
  'use strict';

  return flight.component(terminal);

  function terminal(){

    var header = "> ";
    var uuid;

    this.updateHeader = function(e, data) {
      var uuid = data.uuid;
    };

    this.addOutput = function(e, data) {
      var messageHeader  = data['uuid'] || header;
      $(this.node).append($('<li/>', {text: data.message}));

      var c = $('.output-container')[0];
      c.scrollTop = c.scrollHeight;
    };

    this.after('initialize', function(){
      this.on(document, 'uiAddOutput', this.addOutput);
      this.on(document, 'dataRegistered', this.updateHeader);
    });
  }
});