define(['lib/jsbn',
        'lib/sha1',
        'lib/sjcl',
        'lib/srp-client',
        'lib/jquery.min',
        'lib/flight'],

function() {

  return flight.component(register);

  function register()
  {
    var hashFunc = 'sha-256';
    var group = 2048;
    var uuid;

    this.saveUUID = function(e, data)
    {
      uuid = data.uuid;
    }

    this.srpRegSuccess = function()
    {
      console.log("Yay. SRP Registration success");
      document.location = '/login?new=1';
    };

    this.srpRegFailure = function(e, data)
    {
      console.log("SRP registration failure. Error message: "+ data.error);
      $('#register-error').text('Error creating account: '+ data.error);

      $('#register-btn').attr("disabled", false);
      $('#username').attr("disabled", false);
      $('#password').attr("disabled", false);
    };

    this.registerSRP = function(username, password)
    {
      $('#register-error').text('');
      $('#register-btn').attr("disabled", true);
      $('#username').attr("disabled", true);
      $('#password').attr("disabled", true);

      var srp = new SRPClient(username, password, group, hashFunc);

      var salt = srp.randomHexSalt();
      var verifier = srp.calculateV(salt);

      $.post('/kernel', {
          'type'     : 'srpRegistration',
          'username': username,
          'salt': salt,
          'verifier': verifier.toString(),
          'id': uuid,
      });
    };

    this.after('initialize', function(){
      this.on(document, 'srpRegistrationSuccess', this.srpRegSuccess);
      this.on(document, 'srpRegistrationFailure', this.srpRegFailure);
      this.on(document, 'dataRegistered', this.saveUUID);

      var self = this;

      $('#register-form').submit(function(e) {
        e.preventDefault();
        var username = $("#username").val();
        var password = $("#password").val();
        self.registerSRP(username, password);
      });
    });
  };
});
