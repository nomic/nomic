define(['lib/codemirror',
        'lib/codemirror.lua',
        // Interface only
        'lib/jquery.min',
        'lib/flight'],

function(CodeMirror){
  'use strict';

  return flight.component(editor);

  function editor(){

    var cm; // The main CodeMirror object

    this.storeCode = function(e) {
      this.trigger('dataStorageRequest', { key: 'code', val: cm.getValue() });
    };

    this.sendMessage = function(e) {
      this.trigger('uiAddOutput',   {message: "> " + cm.getValue()});
      this.trigger('dataAddOutput', {message: cm.getValue()});
      this.node.value = '';
    };

    this.handleKeydown = function(e) {
      if (e.metaKey && e.keyCode === 83){
        e.preventDefault();
        e.stopPropagation();
        this.trigger('uiSaveTriggered');
      }
    };

    this.after('initialize', function(){

      cm = CodeMirror.fromTextArea(this.node, {
        mode: "text/x-lua",
        lineNumbers: true,
        theme: "nomic",
      });

      if (localStorage['code']){
        cm.getDoc().setValue(localStorage['code']); // FIXME
      }

      this.on(document, 'keydown',         this.handleKeydown.bind(this));
      this.on(document, 'uiSaveTriggered', this.sendMessage.bind(this));
      this.on(document, 'uiSaveTriggered', this.storeCode.bind(this));
    });
  };
});