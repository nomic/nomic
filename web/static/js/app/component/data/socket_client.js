define(['lib/socket.io.min',
        'lib/flight',],
function(io) {

  'use strict';

  return flight.component(socketClient);

  function socketClient() {

    var socket;
    var assigned_uuid;

    this.onRegister = function(data) {
      assigned_uuid = data['uuid'];
      console.log("Rad, we were assigned UUID \""+ assigned_uuid +"\"");
      this.trigger('dataRegistered', { uuid: assigned_uuid });
    };

    this.receiveLuaResult = function(data) {
      this.trigger('uiAddOutput', { uuid: data.id, message: data.results[0] });
    };

    this.sendLua = function(e, data) {
      socket.emit('kernel', { type: 'lua', message: data.message, id: assigned_uuid });
    };

    this.sendEventToFlight = function(data) {
      this.trigger(data.type, data);
    };

    this.after('initialize', function() {

      var self = this;

      this.on(document, 'dataAddOutput', this.sendLua);

      var hostURL = 'http://' + document.domain + ':' + location.port;
      socket = io.connect(hostURL);

      socket.on('connect', function() {
        socket.emit('join', {username: 'Someone'});
        console.log("Connected to SocketIO backend");
      });

      socket.on('luaResult', self.receiveLuaResult.bind(self));
      socket.on('registered', this.onRegister.bind(self));

      socket.on('srpRegistrationSuccess', self.sendEventToFlight.bind(self));
      socket.on('srpRegistrationFailure', self.sendEventToFlight.bind(self));
      socket.on('srpLoginB',              self.sendEventToFlight.bind(self));
      socket.on('srpLoginM2',             self.sendEventToFlight.bind(self));
      socket.on('srpLoginError',          self.sendEventToFlight.bind(self));
      socket.on('srpLoginOk',             self.sendEventToFlight.bind(self));
      socket.on('srpLogoutOk',             self.sendEventToFlight.bind(self));
    });
  }
});
