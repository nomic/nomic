define([// Interface only
        'lib/flight',],
function() {

  'use strict';

  return flight.component(socketClient);

  function socketClient() {

    // Data should contain at least:
    // { key: ..., val: ... }
    this.set = function(e, data) {
      localStorage.setItem(data.key, data.val);
    };

    this.after('initialize', function() {
      this.on(document, 'dataStorageRequest', this.set.bind(this));
    });
  }
});