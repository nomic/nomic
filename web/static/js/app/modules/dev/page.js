define(['component/data/socket_client',
        'modules/dev/components/ui.buffer',
        //Interface only
        'lib/jquery.min'],

function(SocketClient,
         uiBuffer)
{
  'use strict';

  SocketClient.attachTo(document);
  uiBuffer.attachTo('.buffer');
});