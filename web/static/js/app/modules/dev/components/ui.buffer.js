define(['modules/dev/mixins/ui.draggable',
        //Interface only
        'lib/jquery.min',
        'lib/flight'],

function(withDraggable){
  'use strict';

  return flight.component(buffer, withDraggable);

  function buffer(){
    this.defaultAttrs({

    });

    this.after('initialize', function(){
      this.initializeDraggable();
    });
  }
});
