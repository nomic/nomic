define(['lib/interact'],
function() {
  var withDraggable = function () {

    var GRID_SIZE = 30; // pixels
    var x = 0, y = 0;

    this.uiMakeActive = function(event) {
      this.$node.addClass('dragging');
    };

    this.uiMakeUnactive = function(event) {
      this.$node.removeClass('dragging');
    };

    this.onDragMove = function(event) {
      x += event.dx;
      y += event.dy;
      event.target.setAttribute('data-x', event.pageX|0);
      event.target.setAttribute('data-y', event.pageY|0);

      event.target.style.webkitTransform =
      event.target.style.transform =
          'translate(' + x + 'px, ' + y + 'px)';
    };

    this.onResizeMove = function(event) {
      this.$node.height(this.node.resizeHeight += event.dy);
      this.$node.width(this.node.resizeWidth += event.dx);
    };

    this.initializeDraggable = function() {
      this.node.resizeWidth  = this.$node.width();
      this.node.resizeHeight = this.$node.height();

      this.$node.on('mousedown', this.uiMakeActive.bind(this));
      this.$node.on('mouseup', this.uiMakeUnactive.bind(this));

      interact(this.node)
        .draggable({
          onstart: this.uiMakeActive.bind(this),
          onmove: this.onDragMove.bind(this),
          onend: this.uiMakeUnactive.bind(this),
        })
        .resizable({
          onstart: this.uiMakeActive.bind(this),
          onmove: this.onResizeMove.bind(this),
          onend: this.uiMakeUnactive.bind(this),
        })
        .snap({
          mode: 'grid',
          grid: { x: GRID_SIZE, y: GRID_SIZE },
          range: Infinity
        })
        .on('dragmove', this.onDragMove.bind(this))
        .restrict({
          drag: this.node.parentNode,
          endOnly: true,
        });
    };
  };

  return withDraggable;
 });