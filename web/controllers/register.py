# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for, \
                  current_app

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_register = Blueprint('register', __name__, url_prefix='/register', template_folder='../templates/mod_register')
scss_files = []

# Set the route and accepted methods
@mod_register.route('/', methods=['GET'])
def register():
  return render_template('register.html')

