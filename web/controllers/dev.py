from flask import Blueprint, render_template

mod_dev    = Blueprint('dev', __name__, url_prefix='/dev', template_folder='../templates/mod_dev')
scss_files = []

@mod_dev.route('/', methods=['GET'])
def index():
  print "Hello world!"
  return render_template('index.html')