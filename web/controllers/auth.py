# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for, \
                  current_app

name = 'auth'
# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_auth = Blueprint(name, __name__, url_prefix='/login', template_folder='../templates/mod_auth')

scss_files = []

# Set the route and accepted methods
@mod_auth.route('/', methods=['GET'])
def signin():
  newFlag = False

  if 'new' in request.args:
    newFlag = True

  return render_template("auth.html", newAcct=newFlag)
