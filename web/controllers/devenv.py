# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for, \
                  current_app

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_devenv = Blueprint('devenv', __name__, url_prefix='/devenv', template_folder='../templates/mod_devenv')

scss_files = [
  'css/codemirror.css',
  'css/codemirror.nomic.css'
]

# Set the route and accepted methods
@mod_devenv.route('/', methods=['GET', 'POST'])
def signin():
    return render_template("environment.html")
