import amqplib.client_0_8 as amqp
import json, pdb

conn = amqp.Connection('localhost', userid='guest', password='guest', ssl=False)

ch = conn.channel()

# We need to declare a queue for the kernel to use for replies
replyQueue, _, _ = ch.queue_declare()

def send_to_kernel(data):
	logger.debug("Sending data to kernel: {0}".format(json.dumps(data)))
	data['reply-to'] = replyQueue
	msg = amqp.Message(json.dumps(data), content_type='application/json')
	ch.basic_publish(msg, routing_key="kernel")

def kernel_message_arrived(msg):
	logger.debug('Kernel message: {0}'.format(msg.body))
	body = json.loads(msg.body)

	if body['id'] not in requests:
		logger.error('Received a message we can not reply to (unknown id)')
		return

	logger.debug('Emitting to SocketIO: {0}'.format(body))

	request_emit = requests[body['id']]['emit']
	request_emit(body['type'], body)

def amqp_handler(r, l):
	global requests, logger
	requests = r
	logger = l
	ch.basic_consume(queue=replyQueue, callback=kernel_message_arrived)

	while ch.callbacks:
		ch.wait()
