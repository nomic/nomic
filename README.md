Nomic Game Engine
=================

Architecture
------------

**kernel**: written in rust, executes rule scripts and maintains the object system

**web**: written in Python using Flask/Flask-SocketIO/amqplib, provides a user-friendly code editor/other UI stuff

Setup
-------------

1. install vagrant
2. clone https://gitlab.com/nomic/nomic
3. vagrant up
  * this is going to take some time... Particularly if it has to download the Ubuntu image. Go get some coffee|tea|beer.
4. open http://localhost:8080 in your web browser

### Useful tips & tricks

* The kernel code lives in /vagrant/kernel
  * you can build it with: *cargo build -u* from /vagrant/kernel
  * there's also a command for building it from any directory: *nomic-build-kernel*
* The web components (Flask app & Frontend code) live in /vagrant/web
  * the Flask app will auto-reload when the code changes, no need to compile anything.
* The app is broken into two upstart services:
  1. nomic-kernel
  2. nomic-web
* You can restart both services with the command: *nomic-restart*
* If the RabbitMQ queue gets filled with nasty data you can clear it with: 'sudo /etc/init.d/rabbitmq-server restart'

License
-------------

Nomic Game Engine is licensed under the [3-clause New BSD License](https://gitlab.com/nomic/nomic/blob/master/LICENSE).
